import axios from "axios";
import * as actionTypes from "./actionTypes";
import config from '../../config'



export const fetchDealsSuccess = deals => {
  return {
    type: actionTypes.FETCH_DEALS_SUCCESS,
    deals: deals
  };
};

export const fetchDealsFail = error => {
  return {
    type: actionTypes.FETCH_DEALS_FAIL,
    error: error
  };
};

export const fetchDealsStart = () => {
  return {
    type: actionTypes.FETCH_DEALS_START
  };
};

export const fetchDeals = () => {
  return dispatch => {
    dispatch(fetchDealsStart());
    axios
      .get(`${config.host}/Deals`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        const fetchedDeals = [];
        for (let key in res.data) {

          fetchedDeals.push(res.data[key]);
          
        }
        console.log(fetchedDeals);

        dispatch(fetchDealsSuccess(fetchedDeals));
      })
      .catch(err => {
        dispatch(fetchDealsFail(err));
      });
  };
};
