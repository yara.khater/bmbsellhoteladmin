import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    deals: [],
     loading: true,
      hotels:[],
    hloading: true,
    photo:[],
    phloading:true,
    snames:[],
    sloading:true
};



const fetchDealsStart = ( state, action ) => {
     return updateObject( state, { loading: true } );
    // return state;
};

const fetchDealsSuccess = ( state, action ) => {
   return  updateObject( state, {
        deals: action.deals,
         loading: false
    } 
    
    );

};




const fetchDealsFail = ( state, action ) => {
     return updateObject( state, { loading: false } );
    // return state
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
  
        case actionTypes.FETCH_DEALS_START: return fetchDealsStart( state, action );
        case actionTypes.FETCH_DEALS_SUCCESS: return fetchDealsSuccess( state, action );
        case actionTypes.FETCH_DEALS_FAIL: return fetchDealsFail( state, action );
        default: return state;
    }
};

export default reducer;