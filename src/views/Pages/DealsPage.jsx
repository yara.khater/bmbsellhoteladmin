import React from "react";

import GridItem from "components/Grid/GridItem.jsx";
import Heading from "components/Heading/Heading.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";

import Details from "@material-ui/icons/Details";

import axios from "axios";

import moment from "moment";

import Assignment from "@material-ui/icons/Assignment";

import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";

import { Link } from "react-router-dom";
import config from "../../config";

class DealsPage extends React.Component {
  state = {
    deals: [],
    offset: 0,
    nb: 10,
    hasMore: true,
    selectedState: "All",
    value: moment(new Date()),
    searchString: null
  };

  componentDidMount() {
    console.log("eererererer");
    console.log(process.env);
    // window.onscroll = () => {

    //       if (
    //         window.innerHeight + document.documentElement.scrollTop
    //         === document.documentElement.offsetHeight
    //       ) {
    //        console.log("endddddddd")
    //         if(this.state.hasMore)
    //         this.getDeals();
    //       }
    //     };
    this.getNbDeals();
    // this.getDeals();
  }

  getNbDeals = () => {
    console.log("tokkkk");
    console.log(localStorage.getItem("token").valueOf());
    axios
      .get(`${config.host}/Deals/CountAllDeals`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        this.setState({ nbDeals: res.data });
        console.log(res.data);
        this.getDeals(this.state.selectedState);
      });
  };

  getDeals = state => {
    this.state.value = moment(new Date());

    console.log(state);
    if (this.state.hasMore)
      axios
        .get(`${config.host}/Deals`, {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
          },
          params: {
            offset: this.state.offset,
            nb: this.state.nb,
            state: state === "All" ? null : state
          }
        })
        .then(res => {
          // this.setState({deals:res.deals})
          this.setState(prevState => ({
            deals: [...prevState.deals, ...res.data],
            offset: prevState.offset + 10,
            hasMore: prevState.offset + prevState.nb <= this.state.nbDeals
          }));
        })

        .catch(err => {
          console.log(err);
        });
  };
  onClickAll = event => {
    const id = event.target.id;

    this.setState(
      { selectedState: id, deals: [], offset: 0, nb: 10, hasMore: true },
      () => {
        this.getDeals(id);
      }
    );
  };
  More = () => {
    this.getDeals(this.state.selectedState);
  };

  find = event => {
    var timer;
    if (this.state.timer) clearTimeout(this.state.timer);

    this.setState({ searchString: event.target.value }, () => {
      if (
        this.state.searchString !== "" &&
        this.state.searchString.match(/^ *$/) === null
      ) {
        console.log("INSIDEE");
        timer = window.setTimeout(
          () =>
            axios
              .get(`${config.host}/Deals/Search`, {
                headers: {
                  Authorization: "Bearer " + localStorage.getItem("token")
                },
                params: {
                  searchString: this.state.searchString
                }
              })
              .then(res => {
                console.log("calling");
                if (res.data !== []) this.setState({ deals: res.data });
              })
              .catch(err => {
                console.log(err);
              }),
          200
        );
        console.log(timer);
        this.setState({ timer: timer });
      } else {
        console.log("calling back");
        this.setState(
          prevState => {
            return {
              deals: [],
              offset: 0,
              nb: prevState.offset,
              hasMore: true
            };
          },
          () => {
            timer = window.setTimeout(
              () => this.getDeals(this.state.selectedState),
              500
            );
          }
        );
      }
    });
  };

  handleOptionChange = event => {
    // console.log(event.target.id)
    this.setState({ selectedState: event.target.id }, () => {
      console.log(this.state);
    });
  };

  render() {
    console.log(this.state.selectedState);
    const { classes } = this.props;

    let DealState = {
      0: { state: "Created", color: "blue" },
      1: { state: "Approved", color: "green" },
      2: { state: "Declined", color: "red" },
      3: { state: "Cancelled", color: "yellow" },
      4: { state: "Deleted", color: "grey" },
      5: { state: "Pending Confirmation Email", color: "orange" },
      6: { state: "Bought", color: "purple" },
      7: { state: "Without Payment", color: "black" },
      8: { state: "Pending Payment", color: "brown" },
      9: { state:"Payment Failed",color:"red"},
      10: { state:"Payment Cancelled",color:"yellow"}
    };
    const deals = this.state.deals.map(x => {
      return (
        <TableRow>
          <TableCell>{x.reservation.property.translations[0].name}</TableCell>
          <TableCell>
            {x.reservation.property.translations[0].address}
          </TableCell>
          <TableCell>
            {x.reservation.holderFirstName} {x.reservation.holderLastName}
          </TableCell>
          <TableCell>
            {moment
              .utc(x.reservation.checkInDate)
              .local()
              .format("DD MMM YYYY")}
          </TableCell>

          <TableCell>
            {moment
              .utc(x.reservation.checkOutDate)
              .local()
              .format("DD MMM YYYY")}
          </TableCell>
          <TableCell>
            <b style={{ color: DealState[x.state].color }}>
              {DealState[x.state].state}
            </b>
          </TableCell>
          <TableCell>
            <Link to={`/deals/${x.id}`}>
              <IconButton>
                <Details color="primary" />
              </IconButton>
            </Link>
          </TableCell>
        </TableRow>
      );
    });

    return (
      <React.Fragment>
        <Heading title="Timeline" textAlign="center" />

        <button
          className={
            "btn btn-outline-info mr-2 ml-4 " +
            (this.state.selectedState === "All" ? "active " : "")
          }
          id="All"
          onClick={this.onClickAll}
        >
          All Deals
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Created" ? "active " : "")
          }
          id="Created"
          onClick={this.onClickAll}
        >
          Created Deals
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Approved" ? "active " : "")
          }
          id="Approved"
          onClick={this.onClickAll}
        >
          Approved Deals
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Declined" ? "active " : "")
          }
          id="Declined"
          onClick={this.onClickAll}
        >
          Decliened Deals
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Cancelled" ? "active " : "")
          }
          id="Cancelled"
          onClick={this.onClickAll}
        >
          Cancelled Deals
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Deleted" ? "active " : "")
          }
          id="Deleted"
          onClick={this.onClickAll}
        >
          Deleted Deals
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 mt-2" +
            (this.state.selectedState === "Pending Confirmation Email"
              ? "active "
              : "")
          }
          id="Pending_Confirmation_Email"
          onClick={this.onClickAll}
        >
          Pending Confirmation Email Deals
        </button>

        <button
          className={
            "btn btn-outline-info mr-2 mt-2" +
            (this.state.selectedState === "Bought" ? "active " : "")
          }
          id="Bought"
          onClick={this.onClickAll}
        >
          Bought Deals
        </button>

        <div className="row form-inline">
          <div className="m-auto">
            <input
              className="form-control mr-3  mt-5"
              style={{ height: "30px" }}
              onChange={this.find}
              placeHolder="Search"
            />
          </div>
        </div>

        <GridItem xs={12}>
          <Card>
            <CardHeader icon>
              <CardIcon color="primary">
                <Assignment />
              </CardIcon>
            </CardHeader>
            <CardBody>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <b>Hotel Name</b>
                    </TableCell>
                    <TableCell>
                      <b>Hotel Address</b>
                    </TableCell>

                    <TableCell>
                      <b>Holder Full Name</b>
                    </TableCell>
                    <TableCell>
                      <b>CheckIn Date</b>
                    </TableCell>
                    <TableCell>
                      <b>CheckOut Date</b>
                    </TableCell>
                    <TableCell>
                      <b>State</b>
                    </TableCell>
                  </TableRow>
                  {deals}
                </TableBody>
              </Table>
            </CardBody>
          </Card>
        </GridItem>
        <div className="text-center">
          <a onClick={this.More}>Load more</a>
        </div>
      </React.Fragment>
    );
  }
}
export default DealsPage;
