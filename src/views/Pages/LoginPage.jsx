import React from "react";
import PropTypes from "prop-types";
import axios from "axios";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import { AuthConsumer } from "../../AuthContext";

import loginPageStyle from "assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }

  handleChange = event => {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value }, () => {
      console.log(this.state);
    });
  };

  login = () => {
    axios({
      method: "GET",
      url: "https://localhost:44387/api/Token",
      params: {
        email: this.state.email,
        password: this.state.password,
        screen: "admin"
      }
    })
      .then(r => {
        console.log(r);

        //localStorage.setItem('token', r.data);
      })
      .catch(err => {
        console.log("errrrrr");
        // localStorage.removeItem('token');
        // console.log(localStorage.getItem('token'))

        // dispatch(authFail(err));
      });
  };

  render() {
    const { classes } = this.props;
    return (
      <AuthConsumer>
        {({ isAuth, login, logout }) => (
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={6} md={4}>
                <form>
                  <Card login className={classes[this.state.cardAnimaton]}>
                    <CardHeader
                      className={`${classes.cardHeader} ${classes.textCenter}`}
                      color="rose"
                    >
                      <h4 className={classes.cardTitle}>Log in</h4>
                    </CardHeader>
                    <CardBody>
                      <div>
                        <CustomInput
                          labelText="Email..."
                          id="email"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <Email className={classes.inputAdornmentIcon} />
                              </InputAdornment>
                            ),
                            onChange: this.handleChange,
                            name: "email"
                          }}
                        />
                        <CustomInput
                          labelText="Password"
                          id="password"
                          formControlProps={{
                            fullWidth: true
                          }}
                          inputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <Icon className={classes.inputAdornmentIcon}>
                                  lock_outline
                                </Icon>
                              </InputAdornment>
                            ),
                            onChange: this.handleChange,
                            type: "password",
                            name: "password"
                          }}
                        />
                      </div>
                    </CardBody>
                    <CardFooter className={classes.justifyContentCenter}>
                      <Button
                        color="rose"
                        simple
                        size="lg"
                        block
                        onClick={() =>
                          login(this.state.email, this.state.password)
                        }
                      >
                        Login
                      </Button>
                    </CardFooter>
                  </Card>
                </form>
              </GridItem>
            </GridContainer>
          </div>
        )}
      </AuthConsumer>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(loginPageStyle)(LoginPage);
