import React from "react";

import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Heading from "components/Heading/Heading.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { Link } from "react-router-dom";

import history from "../../history";

import axios from "axios";
import config from "../../config";
import Button from "components/CustomButtons/Button.jsx";
import { Button as ButtonM } from "@material-ui/core";
import moment from "moment";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

class DealPage extends React.Component {
  state = {
    deal: null,
    editing: false,
    priceBeforeEditing: null,
    size: null,
    roomIndex: null,
    selectedOption: "0"
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    console.log("mounttt");
    this.GetDeal(this.props.match.params.id);
    let roomIndexElement = document.getElementById("roomIndex");
    console.log(roomIndexElement);
    roomIndexElement.addEventListener("DOMSubtreeModified", this.myFunc);
  }
  handleOptionChange = changeEvent => {
    this.setState(
      {
        selectedOption: changeEvent.target.value
      },
      () => {
        console.log(this.state);
      }
    );
  };
  GetDeal = id => {
    axios
      .get(`${config.host}/Deals/${id}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ deal: res.data, price: res.data.originalPrice }, () =>
          console.log(this.state)
        );
      });
  };
  GetEmails = () => {
    axios
      .get(`${config.host}/Emails?dealid=${this.props.match.params.id}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        console.log(res.data);

        // this.setState({GetEmails:res.data},() => console.log(this.state))
      });
  };
  ConfirmNameChange = () => {
    console.log("sss", localStorage.getItem("token"));
    axios
      .get(
        `${config.host}/Deals/ConfirmNameChange/${this.props.match.params.id}`,
        {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
          }
        }
      )
      .then(res => {
        console.log(res.data);
        this.setState({ deal: res.data }, () => console.log(this.state));
        history.push("/deals");
      })
      .catch(err => {
        console.log(err);
      });
  };

  SecondChance = () => {
    axios
      .get(`${config.host}/Emails/SecondChance/${this.props.match.params.id}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        // console.log(res.data);
        // this.setState({ deal: res.data }, () => console.log(this.state));
        history.push("/deals");
      })
      .catch(err => {
        console.log(err);
      });
  };

  Decline = () => {
    console.log(this.state);
    this.setState({ open: false });
    axios({
      method: "GET",
      url: `${config.host}/Emails/Declined`,
      params: {
        fullName: this.state.deal.processes[0].user.fullName,
        toEmail: this.state.deal.processes[0].user.email,
        reason: this.state.selectedOption,
        propertyCode: this.state.deal.reservation.property.sourceKey,
        dealId: this.state.deal.id
      },

      headers: {
        Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
      }
    }).then(res => {
      console.log("resssssss");
      console.log(res.data);
    });

    axios
      .get(`${config.host}/Deals/Decline/${this.props.match.params.id}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ deal: res.data }, () => console.log(this.state));
        history.push("/deals");
      });
  };

  Approve = () => {
    axios({
      method: "GET",
      url: `${config.host}/Emails/Approved`,
      params: {
        fullName: this.state.deal.processes[0].user.fullName,
        toEmail: this.state.deal.processes[0].user.email,
        propertyCode: this.state.deal.reservation.property.sourceKey,
        dealId: this.state.deal.id
      },

      headers: {
        Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
      }
    }).then(res => {
      console.log("resssssss");
      console.log(res.data);
    });

    axios
      .get(`${config.host}/Deals/Approve/${this.props.match.params.id}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ deal: res.data }, () => console.log(this.state));
        history.push("/deals");
      });
  };

  lastProcess = deal => {
    if (!deal || !deal.processes || !deal.processes.length) return null;
    let d = deal.processes.find(d => d.state == 0 && d.action ==0 );
    if (typeof d === "undefined") {
      d = deal.processes.find(d => d.state == 7);
    }
    return d;
  };

  SendChangedOriginalPriceEmail = (
    propertyName,
    price,
    priceBeforeEditing,
    dealId,
    sourceKey
  ) => {
    console.log(propertyName, price, priceBeforeEditing, sourceKey);
    console.log("the property coded is ", sourceKey);
    const lastProcess = this.lastProcess(this.state.deal);
    axios
      .get(`${config.host}/Emails/ChangedOriginalPrice`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token")
        },
        params: {
          toEmail: lastProcess.user.email,
          fullName: lastProcess.user.fullName,
          propertyName: propertyName,
          price: price,
          priceBeforeEditing: priceBeforeEditing,
          dealId: dealId,
          propertyCode: sourceKey
        }
      })
      .then(res => {
        console.log(res.data);

        //  this.setState({price:res.data.originalPrice},() => console.log(this.state))
      })
      .catch(err => {
        console.log(err);
      });
  };

  Edit = id => {
    axios
      .get(`${config.host}/Deals/Edit/Price`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token")
        },
        params: {
          id: id,
          originalPrice: this.state.price
        }
      })
      .then(res => {
        console.log(res.data);
        if (
          res.data.originalPrice.toString() !==
          this.state.priceBeforeEditing.toString()
        )
          // console.log("diffff")
          this.SendChangedOriginalPriceEmail(
            this.state.deal.reservation.property.translations[0].name,
            this.state.price,
            this.state.priceBeforeEditing,
            this.state.deal.id,
            this.state.deal.reservation.property.sourceKey
          );
        //  else
        //  console.log("not diffffff")

        //  this.setState({price:res.data.originalPrice},() => console.log(this.state))
      })
      .catch(err => {
        console.log(err);
      });

    this.setState(
      {
        editing: false
      },
      () => {
        console.log(this.state);
      }
    );
  };
  onChange = event => {
    event.preventDefault();
    this.setState(
      {
        price: event.target.value
      },
      () => {
        console.log(this.state);
      }
    );
  };

  myFunc = () => {
    console.log(
      "changededededededededededdedededededdededed",
      this.refs.roomIndex.innerHTML
    );
    console.log(this.refs);
    console.log(this.refs.roomIndex);
    console.log(this.refs.roomIndex.innerHTML);
    // let grid=document.getElementById("grid0");
    //  console.log(grid,grid.getAttribute("xs"))
    this.setState({ size: 6, roomIndex: this.refs.roomIndex.innerHTML });
  };

  render() {
    let tables = [];
    console.log(this.refs.nbRooms);
    if (typeof this.refs.nbRooms !== "undefined") {
      console.log(this.refs.nbRooms.innerHTML.length);
      console.log(this.refs.roomIndex);
      if (this.refs.nbRooms.innerHTML !== "undefined") {
        for (let i = 0; i < this.refs.nbRooms.innerHTML; i++) {
          // console.log(i)
          tables.push(
            // <GridContainer>
            <GridItem
              xs={
                this.state.roomIndex !== null &&
                parseInt(this.state.roomIndex) === i
                  ? this.state.size
                  : 12
              }
              id={"grid" + i}
              className={
                this.state.roomIndex !== null &&
                parseInt(this.state.roomIndex) === i
                  ? "order-2 p-2"
                  : "p-2 order-" + (i + 3)
              }
            >
              <Card>
                <CardBody>
                  <Table
                    id={
                      "table" + i
                      // {"table"+((this.state.roomIndex!==null&&parseInt(this.state.roomIndex))===i?0:this.state.roomIndex!==null&&i===0?this.state.roomIndex:i)
                    }
                  >
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          <b>Hotel Name</b>
                        </TableCell>
                        <TableCell className="hotel_name">
                          hotelname
                          <i
                            className="fa  fa-lg hotel_name_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Holder Full Name</b>
                        </TableCell>
                        <TableCell className="holder_full_name" id={"hfn" + i}>
                          holderfullname
                          <i
                            className="fa  fa-lg holder_full_name_verification "
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>CheckIn Date1</b>
                        </TableCell>
                        <TableCell className="checkin">
                          checkin
                          <span style={{ float: "right" }}>
                            {" "}
                            <i className="fa  fa-lg checkin_verification" />
                          </span>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>CheckOut Date</b>
                        </TableCell>
                        <TableCell className="checkout">
                          checkout{" "}
                          <i
                            className="fa  fa-lg checkout_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Occupancies</b>
                        </TableCell>
                        <TableCell className="occupancies">
                          occupancies
                          <i
                            className="fa  fa-lg  occupancies_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Pure Price</b>
                        </TableCell>
                        <TableCell className="pure_price">pureprice</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>VAT</b>
                        </TableCell>
                        <TableCell className="vat">vat</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Price</b>
                        </TableCell>
                        <TableCell className="price">
                          price
                          <i
                            className="fa  fa-lg price_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Additional Charges</b>
                        </TableCell>
                        <TableCell className="additional_charges">
                          additionalcharges
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Final Price</b>
                        </TableCell>
                        <TableCell className="final_price">
                          finalprice{" "}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Payment Status</b>
                        </TableCell>
                        <TableCell className="payment_status">
                          paymentstatus{" "}
                          <i
                            className="fa  fa-lg payment_status_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Payment Status Description</b>
                        </TableCell>
                        <TableCell className="payment_status_description">
                          paymentstatusdescription{" "}
                          <i
                            className="fa  fa-lg payment_status_description_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Room Name</b>
                        </TableCell>
                        <TableCell className="room_name">
                          roomname{" "}
                          <i
                            className="fa  fa-lg room_name_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <b>Meal PLan</b>
                        </TableCell>
                        <TableCell className="meal_plan">
                          mealplan{" "}
                          <i
                            className="fa  fa-lg meal_plan_verification"
                            style={{ float: "right" }}
                          />
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </CardBody>
              </Card>
            </GridItem>
            // </GridContainer>
          );
          console.log(i);
        }
        console.log(tables);

        for (let i = 0; i < this.refs.nbRooms.innerHTML; i++) {
          //   if(i===parseInt(this.state.roomIndex)){
          //     console.log("hrerere")
          //      let room=tables[i];
          //      tables[i]=tables[0];
          //      tables[0]=room
          // //     tables.splice(i,1);
          // //     tables.splice(0,0,room);
          // //     console.log(tables)
          //    }
        }
      }
    }

    const { classes } = this.props;
    let dealRender = null;
    let deal = this.state.deal;

    if (deal) {
      let approve = (
        <Button color="success" onClick={this.Approve} className="mr-2">
          Approve
        </Button>
      );
      if (deal.state === 1)
        approve = (
          <b className="mr-3 mt-3" style={{ color: "green", fontSize: "18px" }}>
            Approved
          </b>
        );

      let decline = (
        <Button
          color="danger"
          onClick={this.handleClickOpen}
          // onClick={this.Decline}
        >
          Decline
        </Button>
      );
      if (deal.state === 2)
        decline = (
          <b className="mr-3 mt-3" style={{ color: "red", fontSize: "18px" }}>
            Declined{" "}
          </b>
        );

      let confirmNameChange = (
        <Button color="primary" onClick={this.ConfirmNameChange}>
          Confirm Name Change
        </Button>
      );
      let secondChanche = (
        <Button color="primary" onClick={this.SecondChance}>
          Send Second Chance
        </Button>
      );

      if (deal.state === 6)
        decline = (
          <b className="mr-3 mt-3" style={{ color: "red", fontSize: "18px" }}>
            Bought{" "}
          </b>
        );

      if (deal.state === 4) {
        approve = decline = null;
      }
      if (deal.state !== 5) {
        confirmNameChange = null;
        secondChanche = null;
      }
      const lastProcess = this.lastProcess(deal); //getting the first one(who created it)

      // let originalPrice = (
      //   {deal.originalPrice}
      // );

      // if (this.state.editing === true)
      //   price = (
      //     <div className="row">
      //       <input
      //         type="number"
      //         className="form-control col-8 ml-2"
      //         value={this.state.price}
      //         onChange={this.onChange}
      //       />
      //       <button
      //         className="col-3 btn btn-primary btn-sm ml-2"
      //         onClick={this.Edit}
      //       >
      //         Done
      //       </button>
      //     </div>
      //   );
      console.log(deal);

      if (
        deal.reservation.platform !== null &&
        deal.reservation.platform.site === "Booking.com"
      )
        dealRender = (
          <Card>
            <CardBody>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <b>Hotel Name</b>
                    </TableCell>
                    <TableCell id="user_hotel_name">
                      {deal.reservation.property.translations[0].name}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Hotel Address</b>
                    </TableCell>
                    <TableCell>
                      {deal.reservation.property.translations[0].address}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Holder First Name</b>
                    </TableCell>
                    <TableCell id="user_holder_first_name">
                      {deal.reservation.holderFirstName}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Holder Last Name</b>
                    </TableCell>
                    <TableCell id="user_holder_last_name">
                      {deal.reservation.holderLastName}
                    </TableCell>
                  </TableRow>
                  {deal.reservation.newHolderFirstName && (
                    <TableRow>
                      <TableCell>
                        <b>New Holder First Name</b>
                      </TableCell>
                      <TableCell>
                        {deal.reservation.newHolderFirstName}
                      </TableCell>
                    </TableRow>
                  )}
                  {deal.reservation.newHolderLastName && (
                    <TableRow>
                      <TableCell>
                        <b>New Holder Last Name</b>
                      </TableCell>
                      <TableCell>
                        {deal.reservation.newHolderLastName}
                      </TableCell>
                    </TableRow>
                  )}
                  <TableRow>
                    <TableCell>
                      <b>CheckIn Date2</b>
                    </TableCell>
                    <TableCell id="user_checkin">
                      {moment
                        .utc(deal.reservation.checkInDate)
                        .local()
                        .format("DD MMM YYYY")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>CheckOut Date</b>
                    </TableCell>
                    <TableCell id="user_checkout">
                      {moment
                        .utc(deal.reservation.checkOutDate)
                        .local()
                        .format("DD MMM YYYY")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Adults</b>
                    </TableCell>
                    <TableCell id="user_adults">
                      {deal.reservation.numberOfAdults}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Children</b>
                    </TableCell>
                    <TableCell id="user_children">
                      {deal.reservation.numberOfChildren}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Room Name</b>
                    </TableCell>
                    <TableCell id="user_room_name">
                      {deal.reservation.roomType}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Room Board</b>
                    </TableCell>
                    <TableCell id="user_room_board">
                      {deal.reservation.roomBoard}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Price</b>
                    </TableCell>
                    <TableCell id="user_price">{deal.price}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Original Price</b>
                    </TableCell>

                    <TableCell id="user_original_price">
                      {!this.state.editing ? (
                        this.state.price
                      ) : (
                        <div className="row">
                          <input
                            type="number"
                            className="form-control col-8 ml-2"
                            //  defaultValue={deal.originalPrice}
                            // defaultValue={this.state.price}
                            value={this.state.price}
                            onChange={this.onChange}
                          />
                          <button
                            className="col-3 btn btn-primary btn-sm ml-2"
                            onClick={() => this.Edit(deal.id)}
                          >
                            Done
                          </button>
                        </div>
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Reservation</b>
                    </TableCell>
                    <TableCell>{deal.reservation.reservationType}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Source</b>
                    </TableCell>
                    <TableCell id="source">
                      {deal.reservation.platform !== null
                        ? deal.reservation.platform.site
                        : deal.reservation.agencyName !== null
                        ? deal.reservation.agencyName
                        : null}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Key 1</b>
                    </TableCell>
                    <TableCell id="key1">
                      {deal.reservation.platformKey1}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>key 2(optional)</b>
                    </TableCell>
                    <TableCell id="key2">
                      {deal.reservation.platformKey2}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>User FullName</b>
                    </TableCell>
                    <TableCell>{lastProcess.user.fullName}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>User Email</b>
                    </TableCell>
                    <TableCell>{lastProcess.user.email}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Reservation Email</b>
                    </TableCell>
                    <TableCell>{deal.reservation.email}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Emails</b>
                    </TableCell>
                    <TableCell>
                      <Link to={`/emails/${deal.id}`}>--></Link>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              {(deal.state === 0 ||
                deal.state === 1 ||
                lastProcess.state === 2) && (
                <div className="mt-5 float-right row">
                  {approve}
                  {decline}

                  <p
                    onClick={() =>
                      this.setState(
                        { editing: true, priceBeforeEditing: this.state.price },
                        () => {
                          console.log(this.state);
                        }
                      )
                    }
                    className="mr-2"
                  >
                    Edit Price
                  </p>
                </div>
              )}

              {deal.state === 5 && (
                <div className="mt-5 float-right row">
                  {confirmNameChange}
                  {"  "}
                  {secondChanche}
                </div>
              )}
            </CardBody>
          </Card>
        );
      else
        dealRender = (
          <Card>
            <CardBody>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <b>Hotel Name</b>
                    </TableCell>
                    <TableCell id="user_hotel_name">
                      {deal.reservation.property.translations[0].name}
                    </TableCell>
                    <TableCell>
                      <b>Hotel Address</b>
                    </TableCell>
                    <TableCell>
                      {deal.reservation.property.translations[0].address}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Holder First Name</b>
                    </TableCell>
                    <TableCell id="user_holder_first_name">
                      {deal.reservation.holderFirstName}
                    </TableCell>
                    <TableCell>
                      <b>Holder Last Name</b>
                    </TableCell>
                    <TableCell id="user_holder_last_name">
                      {deal.reservation.holderLastName}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Holder Phone Extension</b>
                    </TableCell>
                    <TableCell id="holder_phone_extension">
                      {deal.contactPhoneExtension}
                    </TableCell>
                    <TableCell>
                      <b>Holder Phone Number</b>
                    </TableCell>
                    <TableCell id="holder_contact_number">
                      {deal.contactNumber}
                    </TableCell>
                  </TableRow>
                  {deal.reservation.newHolderFirstName && (
                    <TableRow>
                      <TableCell>
                        <b>New Holder First Name</b>
                      </TableCell>
                      <TableCell>
                        {deal.reservation.newHolderFirstName}
                      </TableCell>
                      <TableCell>
                        <b>New Holder Last Name</b>
                      </TableCell>
                      <TableCell>
                        {deal.reservation.newHolderLastName}
                      </TableCell>
                    </TableRow>
                  )}
                  <TableRow>
                    <TableCell>
                      <b>CheckIn Date</b>
                    </TableCell>
                    <TableCell id="user_checkin">
                      {moment
                        .utc(deal.reservation.checkInDate)
                        .local()
                        .format("DD MMM YYYY")}
                    </TableCell>
                    <TableCell>
                      <b>CheckOut Date</b>
                    </TableCell>
                    <TableCell id="user_checkout">
                      {moment
                        .utc(deal.reservation.checkOutDate)
                        .local()
                        .format("DD MMM YYYY")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Adults</b>
                    </TableCell>
                    <TableCell id="user_adults">
                      {deal.reservation.numberOfAdults}
                    </TableCell>
                    <TableCell>
                      <b>Children</b>
                    </TableCell>
                    <TableCell id="user_children">
                      {deal.reservation.numberOfChildren}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Room Name</b>
                    </TableCell>
                    <TableCell id="user_room_name">
                      {deal.reservation.roomType}
                    </TableCell>
                    <TableCell>
                      <b>Room Board</b>
                    </TableCell>
                    <TableCell id="user_room_board">
                      {deal.reservation.roomBoard}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Price</b>
                    </TableCell>
                    <TableCell id="user_price">{deal.price}</TableCell>
                    <TableCell>
                      <b>Original Price</b>
                    </TableCell>
                    <TableCell id="user_original_price">
                      {!this.state.editing ? (
                        this.state.price
                      ) : (
                        <div className="row">
                          <input
                            type="number"
                            className="form-control col-8 ml-2"
                            //  defaultValue={deal.originalPrice}
                            // defaultValue={this.state.price}
                            value={this.state.price}
                            onChange={this.onChange}
                          />
                          <button
                            className="col-3 btn btn-primary btn-sm ml-2"
                            onClick={() => this.Edit(deal.id)}
                          >
                            Done
                          </button>
                        </div>
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Reservation</b>
                    </TableCell>
                    <TableCell>{deal.reservation.reservationType}</TableCell>
                    <TableCell>
                      <b>Source</b>
                    </TableCell>
                    <TableCell id="source">
                      {deal.reservation.platform !== null
                        ? deal.reservation.platform.site
                        : deal.reservation.agencyName !== null
                        ? deal.reservation.agencyName
                        : null}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Key 1</b>
                    </TableCell>
                    <TableCell id="key1">
                      {deal.reservation.platformKey1}
                    </TableCell>
                    <TableCell>
                      <b>key 2(optional)</b>
                    </TableCell>
                    <TableCell id="key2">
                      {deal.reservation.platformKey2}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>User FullName</b>
                    </TableCell>
                    <TableCell>{lastProcess.user.fullName}</TableCell>
                    <TableCell>
                      <b>User Email</b>
                    </TableCell>
                    <TableCell>{lastProcess.user.email}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>User Number Extention</b>
                    </TableCell>
                    <TableCell>{lastProcess.user.phoneExtension}</TableCell>
                    <TableCell>
                      <b>User Number</b>
                    </TableCell>
                    <TableCell>{lastProcess.user.phoneNumber}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>Reservation Email</b>
                    </TableCell>
                    <TableCell>{deal.reservation.email}</TableCell>
                    <TableCell>
                      <b>Emails</b>
                    </TableCell>
                    <TableCell>
                      <Link to={`/emails/${deal.id}`}>--></Link>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>File</b>
                    </TableCell>
                    <TableCell>
                      <a
                        target="_blank"
                        href={deal.originalReservationFilePath + ""}
                      >
                        Link{" "}
                      </a>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              {(deal.state === 0 ||
                deal.state === 1 ||
                lastProcess.state === 2) && (
                <div className="mt-5 float-right row">
                  {approve}
                  {decline}

                  <p
                    onClick={() =>
                      this.setState(
                        { editing: true, priceBeforeEditing: this.state.price },
                        () => {
                          console.log(this.state);
                        }
                      )
                    }
                    className="mr-2"
                  >
                    Edit Price
                  </p>
                </div>
              )}

              {deal.state === 5 && (
                <div className="mt-5 float-right row">
                  {confirmNameChange}
                  {"  "}
                  {secondChanche}
                </div>
              )}
            </CardBody>
          </Card>
        );
    }
    return (
      <React.Fragment>
        <Heading title="Timeline" textAlign="center" />
        <GridContainer className="d-flex  flex-wrap">
          <GridItem
            xs={
              this.state.deal !== null &&
              this.state.deal.reservation.platform !== null &&
              this.state.deal.reservation.platform.site === "Booking.com"
                ? 6
                : 12
            }
            className="p-2 order-1"
          >
            {dealRender}
          </GridItem>
          {/* <div > */}
          {tables}
          {/* </div> */}
        </GridContainer>
        <div id="lonely_planet_results">
          <div id="nbRooms" ref="nbRooms" />
          <div id="roomIndex" ref="roomIndex" />

          {/* {typeof this.refs.nbRooms!=='undefined' && this.refs.nbRooms.innerHTML.length>0 && this.refs.nbRooms.innerHTML!=="undefined"
          && 
            <GridContainer>
          <GridItem xs={12}>
            <Card>
              <CardBody>
                <Table >
                  <TableBody>
                    <TableRow>
                      <TableCell>
                        <b>Hotel Name</b>
                      </TableCell>
                      <TableCell id="hotel_name">
                        hotelname
                        <i
                          id="hotel_name_verification"
                          className="fa  fa-lg "
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>Holder Full Name</b>
                      </TableCell>
                      <TableCell id="holder_full_name">
                        holderfullname
                        <i
                          id="holder_full_name_verification"
                          className="fa  fa-lg"
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>CheckIn Date</b>
                      </TableCell>
                      <TableCell id="checkin">
                        checkin
                        <i
                          id="checkin_verification"
                          holder_full_name
                          className="fa  fa-lg"
                        />
                      </TableCell>
                      <TableCell>
                        <b>CheckOut Date</b>
                      </TableCell>
                      <TableCell id="checkout">
                        checkout{" "}
                        <i id="checkout_verification" className="fa  fa-lg" />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>Occupancies</b>
                      </TableCell>
                      <TableCell id="occupancies">
                        occupancies
                        <i
                          id="occupancies_verification"
                          className="fa  fa-lg"
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>Pure Price</b>
                      </TableCell>
                      <TableCell id="pure_price">pureprice</TableCell>
                      <TableCell>
                        <b>VAT</b>
                      </TableCell>
                      <TableCell id="vat">vat</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>Price</b>
                      </TableCell>
                      <TableCell id="price">
                        price
                        <i id="price_verification" className="fa  fa-lg" />
                      </TableCell>
                      <TableCell>
                        <b>Additional Charges</b>
                      </TableCell>
                      <TableCell id="additional_charges">
                        additionalcharges
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>FINAL PRICE</b>
                      </TableCell>
                      <TableCell id="final_price">finalprice </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>Payment Status</b>
                      </TableCell>
                      <TableCell id="payment_status">
                        paymentstatus{" "}
                        <i
                          id="payment_status_verification"
                          className="fa  fa-lg"
                        />
                      </TableCell>
                      <TableCell>
                        <b>Payment Status Description</b>
                      </TableCell>
                      <TableCell id="payment_status_description">
                        paymentstatusdescription{" "}
                        <i
                          id="payment_status_description_verification"
                          className="fa  fa-lg"
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <b>Room Name</b>
                      </TableCell>
                      <TableCell id="room_name">
                        roomname{" "}
                        <i id="room_name_verification" className="fa  fa-lg" />
                      </TableCell>
                      <TableCell>
                        <b>Meal PLan</b>
                      </TableCell>
                      <TableCell id="meal_plan">
                        mealplan{" "}
                        <i id="meal_plan_verification" className="fa  fa-lg" />
                      </TableCell>
                    </TableRow>
                    {/* <TableRow>
                  <TableCell >
                    <b>Key 1</b>
                  </TableCell>
                  <TableCell id="key1">{deal.reservation.platformKey1}</TableCell>
                  <TableCell >
                    <b>key 2(optional)</b>
                  </TableCell>
                  <TableCell id="key2">
                  {deal.reservation.platformKey2}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>User FullName</b>
                  </TableCell>
                  <TableCell>{lastProcess.user.fullName}</TableCell>
                  <TableCell>
                    <b>User Email</b>
                  </TableCell>
                  <TableCell>{lastProcess.user.email}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Reservation Email</b>
                  </TableCell>
                  <TableCell>{deal.reservation.email}</TableCell>
                  <TableCell>
                    <b>Emails</b>
                  </TableCell>
                  <TableCell>
                    <Link to={`/emails/${deal.id}`}>--></Link>
                  </TableCell>
                </TableRow> */}
          {/* </TableBody>
                </Table>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>} */}
        </div>

        <div>
          {/* <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
        Open form dialog
      </Button> */}
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">
              Why Did You Decline?
            </DialogTitle>
            <DialogContent>
              <input
                type="radio"
                name="gender"
                value="0"
                onChange={this.handleOptionChange}
                checked={this.state.selectedOption === "0"}
              />{" "}
              The reservation is refundable or not fully prepaid <br />
              <input
                type="radio"
                name="gender"
                value="1"
                onChange={this.handleOptionChange}
                checked={this.state.selectedOption === "1"}
              />{" "}
              The confirmation number is wrong
              <br />
              <input
                type="radio"
                name="gender"
                value="2"
                onChange={this.handleOptionChange}
                checked={this.state.selectedOption === "2"}
              />
              Wrong info related to check in/check out dates, room type…
              <br />{" "}
              <input
                type="radio"
                name="gender"
                value="3"
                onChange={this.handleOptionChange}
                checked={this.state.selectedOption === "3"}
              />
              Cannot change the reservation name
              <br />{" "}
              <input
                type="radio"
                name="gender"
                value="4"
                onChange={this.handleOptionChange}
                checked={this.state.selectedOption === "4"}
              />
              The reservation is not transferable
              <br />{" "}
              <input
                type="radio"
                name="gender"
                value="5"
                onChange={this.handleOptionChange}
                checked={this.state.selectedOption === "5"}
              />
              The reservation does not exist
              <br />
            </DialogContent>
            <DialogActions>
              <ButtonM size="small" onClick={this.handleClose} color="primary">
                Cancel
              </ButtonM>
              <ButtonM size="small" onClick={this.Decline} color="primary">
                Done
              </ButtonM>
            </DialogActions>
          </Dialog>
        </div>
      </React.Fragment>
    );
  }
}

export default DealPage;
