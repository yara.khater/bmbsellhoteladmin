import React from "react";

import config from "../../config";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Heading from "components/Heading/Heading.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import history from "../../history";
import CardHeader from "components/Card/CardHeader.jsx";
import Details from "@material-ui/icons/Details";
import { Link } from "react-router-dom";
import Assignment from "@material-ui/icons/Assignment";

import CardIcon from "components/Card/CardIcon.jsx";
import axios from "axios";
//import { Button } from "@material-ui/core";
import Button from "components/CustomButtons/Button.jsx";
import moment from "moment";
import { TableHead, Paper } from "@material-ui/core";

class DealPage extends React.Component {
  state = {
    emails: [],
    selectedState: "All"
  };

  componentDidMount() {
    axios
      .get(`${config.host}/Emails/ByState`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        },
        params: {
          state: "All"
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ emails: res.data }, () => console.log(this.state));
      });

    // this.getDeals();
  }

  getEmails = state => {
    axios
      .get(`${config.host}/Emails/ByState`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        },
        params: {
          state: state
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ emails: res.data }, () => console.log(this.state));
      });
  };

  onClickAll = event => {
    const id = event.target.id;

    this.setState(
      { selectedState: id, emails: [], offset: 0, nb: 10, hasMore: true },
      () => {
        this.getEmails(id);
      }
    );
  };
  More = () => {
    this.getEmails(this.state.selectedState);
  };
  render() {
    const { classes } = this.props;
    let deal = null;

    // if(x){

    //   let approve = <Button color="success" onClick={this.Approve} className="mr-2">Approve</Button>
    //   if(x.state===1)
    //   approve=<b className="mr-3 mt-3" style={{ color: "green" , fontSize:"18px" }}>Approved</b>

    //   let decline =  <Button color="danger" onClick={this.Decline}>Decline</Button>
    //   if(x.state===2)
    //   decline=<b className="mr-3 mt-3" style={{ color: "red" , fontSize:"18px" }}>Declined </b>

    //   if(x.state===4){
    //     approve=decline=null
    //   }
    const emails = this.state.emails.map(e => {
      return (
        <TableRow style={{ overflowX: "scroll" }}>
          <TableCell>
            <Link to={`/deals/${e.deal.id}`}>
              <IconButton>
                <Details color="primary" />
              </IconButton>
            </Link>
          </TableCell>
          <TableCell>{e.fromEmail}</TableCell>
          {/* <TableCell>{e.toEmail}</TableCell> */}
          <TableCell>
            {moment
              .utc(e.dateSent)
              .local()
              .format("DD MMM YYYYTHH:mm:ss.SSS")}
          </TableCell>
          <TableCell>{e.subject}</TableCell>
          <TableCell>
            <a
              target="_blank"
              href={`${config.host}/emails/message?messageId=${e.id}`}
            >
              Email Message Details
            </a>
          </TableCell>
        </TableRow>
      );
    });

    return (
      <React.Fragment>
        {/* <Heading title="Timeline" textAlign="center" />
        <GridContainer>
          <GridItem xs={12}>
        
          {deal}
          </GridItem>
        </GridContainer> */}
        {/* <div style={{overflowX:"scroll"}}> */}

        <button
          className={
            "btn btn-outline-info mr-2 ml-4 " +
            (this.state.selectedState === "All" ? "active " : "")
          }
          id="All"
          onClick={this.onClickAll}
        >
          All Emails
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Original" ? "active " : "")
          }
          id="Original"
          onClick={this.onClickAll}
        >
          Original Reservation Email
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Changed" ? "active " : "")
          }
          id="Changed"
          onClick={this.onClickAll}
        >
          Changed Reservation Email
        </button>

        <GridItem xs={12}>
          <Card>
            <CardHeader icon>
              <CardIcon color="primary">
                <Assignment />
              </CardIcon>
            </CardHeader>
            <CardBody>
              <Paper
                style={{
                  width: "100%",
                  overflowX: "auto"
                }}
              >
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell />
                      <TableCell>
                        <b>From</b>
                      </TableCell>
                      {/* <TableCell>
                        <b>To</b>
                      </TableCell> */}

                      <TableCell>
                        <b>Date Sent</b>
                      </TableCell>
                      <TableCell>
                        <b>Subject</b>
                      </TableCell>
                      <TableCell>
                        <b>Body</b>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>{emails}</TableBody>
                </Table>
              </Paper>
            </CardBody>
          </Card>
        </GridItem>
        {/* <div className="text-center">
          <a onClick={this.More}>Load more</a>
        </div> */}
        {/* </div> */}
      </React.Fragment>
    );
  }
}

export default DealPage;
