import React from "react";

import GridItem from "components/Grid/GridItem.jsx";
import Heading from "components/Heading/Heading.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";

import Details from "@material-ui/icons/Details";

import axios from "axios";

import moment from "moment";

import Assignment from "@material-ui/icons/Assignment";

import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";

import { Link } from "react-router-dom";
import config from "../../config";

class BookingsPage extends React.Component {
  state = {
    bookings: [],
    offset: 0,
    nb: 10,
    hasMore: true,
    selectedState: "All",
    value: moment(new Date()),
    searchString: null
  };

  componentDidMount() {
    console.log("eererererer");
    console.log(process.env);
    // window.onscroll = () => {

    //       if (
    //         window.innerHeight + document.documentElement.scrollTop
    //         === document.documentElement.offsetHeight
    //       ) {
    //        console.log("endddddddd")
    //         if(this.state.hasMore)
    //         this.getDeals();
    //       }
    //     };
    this.getNbBookings();
    // this.getDeals();
  }

  getNbBookings = () => {
    console.log("tokkkk");
    console.log(localStorage.getItem("token").valueOf());
    axios
      .get(`${config.host}/Bookings/CountAllBookings`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        this.setState({ nbBookings: res.data });
        console.log(res.data);
        this.getBookings(this.state.selectedState);
      });
  };

  getBookings = state => {
    this.state.value = moment(new Date());

    console.log(state);
    if (this.state.hasMore)
      axios
        .get(`${config.host}/Bookings`, {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
          },
          params: {
            offset: this.state.offset,
            nb: this.state.nb,
            state: state === "All" ? null : state
          }
        })
        .then(res => {
          // this.setState({deals:res.deals})
          console.log(res.data)
          this.setState(prevState => ({
            bookings: [...prevState.bookings, ...res.data],
            offset: prevState.offset + 10,
            hasMore: prevState.offset + prevState.nb <= this.state.nbBookings
          }));
        })

        .catch(err => {
          console.log(err);
        });
  };
  onClickAll = event => {
    const id = event.target.id;

    this.setState(
      { selectedState: id, bookings: [], offset: 0, nb: 10, hasMore: true },
      () => {
        this.getBookings(id);
      }
    );
  };
  More = () => {
    this.getBookings(this.state.selectedState);
  };

  find = event => {
    var timer;
    if (this.state.timer) clearTimeout(this.state.timer);

    this.setState({ searchString: event.target.value }, () => {
      if (
        this.state.searchString !== "" &&
        this.state.searchString.match(/^ *$/) === null
      ) {
        console.log("INSIDEE");
        timer = window.setTimeout(
          () =>
            axios
              .get(`${config.host}/Bookings/Search`, {
                headers: {
                  Authorization: "Bearer " + localStorage.getItem("token")
                },
                params: {
                  searchString: this.state.searchString
                }
              })
              .then(res => {
                console.log("calling");
                if (res.data !== []) this.setState({ bookings: res.data });
              })
              .catch(err => {
                console.log(err);
              }),
          200
        );
        console.log(timer);
        this.setState({ timer: timer });
      } else {
        console.log("calling back");
        this.setState(
          prevState => {
            return {
              Bookings: [],
              offset: 0,
              nb: prevState.offset,
              hasMore: true
            };
          },
          () => {
            timer = window.setTimeout(
              () => this.getBookings(this.state.selectedState),
              200
            );
          }
        );
      }
    });
  };

  handleOptionChange = event => {
    // console.log(event.target.id)
    this.setState({ selectedState: event.target.id }, () => {
      console.log(this.state);
    });
  };

  render() {
    console.log(this.state.selectedState);
    const { classes } = this.props;

    // let BookingState = {
    //   0: { state: "Done", color: "blue" },
    //   5: { state: "Done", color: "green" },
    //   6: { state: "Booking_Failed", color: "red" },
    //   7: { state: "Booking_Cancelled", color: "yellow" },
    //   8: { state: "Booking_Expired", color: "red" }
     
    // };

    let BookingState = {
      0: { state: "Booking_Created", color: "blue" },
      5: { state: "Booking_Confirmed", color: "green" },
      6: { state: "Booking_Failed", color: "red" },
      7: { state: "Booking_Cancelled", color: "yellow" },
      8: { state: "Booking_Expired", color: "red" },
      9: { state: "Booking_Updated", color: "purple" },
      1: { state: "Payment_Started", color: "purple" },
      2: { state: "Payment_Confirmed", color: "purple" },
      3: { state: "Payment_Failed", color: "purple" },
      4: { state: "Payment_Cancelled", color: "purple" },

      
    };
    const bookings = this.state.bookings.map(x => {
        console.log(x)
      return (
        <TableRow>
          <TableCell>{x.reservation.property.translations[0].name}</TableCell>
          <TableCell>
            {x.reservation.property.translations[0].address}
          </TableCell>
          <TableCell>
            {x.reservation.holderFirstName} {x.reservation.holderLastName}
          </TableCell>
          <TableCell>
            {moment
              .utc(x.reservation.checkInDate)
              .local()
              .format("DD MMM YYYY")}-{moment
                .utc(x.reservation.checkOutDate)
                .local()
                .format("DD MMM YYYY")}
          </TableCell>

          {/* <TableCell>
            
          </TableCell> */}
          <TableCell>
            <b style={{ color: BookingState[x.state].color }}>
              {BookingState[x.state].state}
            </b>
          </TableCell>
          <TableCell>
            <b style={{ color: x.paymentState =="Paid"?"green":x.paymentState=="Payment Rejected"?"red":x.paymentState=="Payment Refunded"?"yellow":"blue" }}>
             {x.paymentState}
            </b>
          </TableCell>
          <TableCell>
            <Link to={`/bookings/${x.id}`}>
              <IconButton>
                <Details color="primary" />
              </IconButton>
            </Link>
          </TableCell>
        </TableRow>
      );
    });

    return (
      <React.Fragment>
        <Heading title="Timeline" textAlign="center" />

        <button
          className={
            "btn btn-outline-info mr-2 ml-4 " +
            (this.state.selectedState === "All" ? "active " : "")
          }
          id="All"
          onClick={this.onClickAll}
        >
          All Bookings
        </button>

        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Created_Bookig" ? "active " : "")
          }
          id="Booking_Created"
          onClick={this.onClickAll}
        >
          Created Bookings
        </button>

        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Booking_Confirmed" ? "active " : "")
          }
          id="Booking_Confirmed"
          onClick={this.onClickAll}
        >
          Confirmed Bookings
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Booking_Failed" ? "active " : "")
          }
          id="Booking_Failed"
          onClick={this.onClickAll}
        >
          Failed Bookings
        </button>
        <button
          className={
            "btn btn-outline-info mr-2 " +
            (this.state.selectedState === "Cancelled_Bookig" ? "active " : "")
          }
          id="Booking_Cancelled"
          onClick={this.onClickAll}
        >
          Cancelled Bookings
        </button>
       

        <div className="row form-inline">
          <div className="m-auto">
            <input
              className="form-control mr-3  mt-5"
              style={{ height: "30px" }}
              onChange={this.find}
              placeHolder="Search"
            />
          </div>
        </div>

        <GridItem xs={12}>
          <Card>
            <CardHeader icon>
              <CardIcon color="primary">
                <Assignment />
              </CardIcon>
            </CardHeader>
            <CardBody>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <b>Hotel Name</b>
                    </TableCell>
                    <TableCell>
                      <b>Hotel Address</b>
                    </TableCell>

                    <TableCell>
                      <b>Holder Full Name</b>
                    </TableCell>
                    <TableCell>
                      <b>CheckIn-CheckOut Date</b>
                    </TableCell>
                    {/* <TableCell>
                      <b>CheckOut Date</b>
                    </TableCell> */}
                    <TableCell>
                      <b>Booking State</b>
                    </TableCell>
                    <TableCell>
                      <b>Payment State</b>
                    </TableCell>
                    <TableCell>
                      <b>Booking Info</b>
                    </TableCell>
                  </TableRow>
                  {bookings}
                </TableBody>
              </Table>
            </CardBody>
          </Card>
        </GridItem>
        <div className="text-center">
          <a onClick={this.More}>Load more</a>
        </div>
      </React.Fragment>
    );
  }
}
export default BookingsPage;
