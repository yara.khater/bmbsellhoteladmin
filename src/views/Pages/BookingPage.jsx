import React from "react";

import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Heading from "components/Heading/Heading.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import { Link } from "react-router-dom";

import history from "../../history";

import axios from "axios";
import config from '../../config'
import Button from "components/CustomButtons/Button.jsx";
import moment from "moment";

class BookingPage extends React.Component {
  state = {
    booking: null,
    editing:false
  };

  componentDidMount() {
    console.log("mounttt")
    this.GetBooking(this.props.match.params.id);
    
  }

  GetBooking = id => {
    axios
      .get(`${config.host}/Bookings/${id}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ booking: res.data,price:res.data.originalPrice }, () => console.log(this.state));
      });
  };
//   GetEmails = () => {
//     axios
//       .get(
//         `${config.host}/Emails?dealid=${
//           this.props.match.params.id
//         }`,
//         {
//           headers: {
//             Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
//           }
//         }
//       )
//       .then(res => {
//         console.log(res.data);

//         // this.setState({GetEmails:res.data},() => console.log(this.state))
//       });
//   };
//   Decline = () => {
//     axios({
//       method: "GET",
//       url: `${config.host}/Emails/Declined`,
//       params: {
//         fullName: this.state.deal.processes[0].user.fullName,
//         toEmail: this.state.deal.processes[0].user.email
//       },

//       headers: {
//         Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
//       }
//     }).then(res => {
//       console.log("resssssss");
//       console.log(res.data);
//     });

//     axios
//       .get(
//         `${config.host}/Deals/Decline/${
//           this.props.match.params.id
//         }`,
//         {
//           headers: {
//             Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
//           }
//         }
//       )
//       .then(res => {
//         console.log(res.data);
//         this.setState({ deal: res.data }, () => console.log(this.state));
//         history.push("/deals");
//       });
//   };

//   Approve = () => {
//     axios({
//       method: "GET",
//       url: `${config.host}/Emails/Approved`,
//       params: {
//         fullName: this.state.deal.processes[0].user.fullName,
//         toEmail: this.state.deal.processes[0].user.email
//       },

//       headers: {
//         Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
//       }
//     }).then(res => {
//       console.log("resssssss");
//       console.log(res.data);
//     });

//     axios
//       .get(
//         `${config.host}/Deals/Approve/${
//           this.props.match.params.id
//         }`,
//         {
//           headers: {
//             Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
//           }
//         }
//       )
//       .then(res => {
//         console.log(res.data);
//         this.setState({ deal: res.data }, () => console.log(this.state));
//         history.push("/deals");
//       });
//   };

  lastProcess = booking => {
    if (!booking || !booking.processes || !booking.processes.length) return  null;
    return booking.processes.find(d => d.state == 0);
  }
//   Edit = (id) => {
//     axios
//     .get(`${config.host}/Deals/Edit/Price`, {
//       headers: {
//         Authorization: "Bearer " + localStorage.getItem("token") 
//       },
//       params: {
//         id: id,
//         originalPrice: this.state.price
//       }
//     })
//     .then(res => {
//       console.log(res.data);
     
//       //  this.setState({price:res.data.originalPrice},() => console.log(this.state))
      
//     })
//     .catch(err => {
       
//       console.log(err);
//     });
    
//     this.setState(
//       {
//         editing: false
        
//       },
//       () => {
//         console.log(this.state);
//       }
//     );
//   };
  onChange = event => {
    event.preventDefault();
    this.setState(
      {
        price: event.target.value
      },
      () => {
        console.log(this.state);
      }
    );
  };
  render() {
    const { classes } = this.props;
    let bookingRender = null;
    let booking = this.state.booking;

    if (booking) {

let rooms=[];

booking.reservation.rooms.map((room,r)=>{
  rooms.push(<TableRow>
  <TableCell>
    <b>Room {r+1} Name</b>
  </TableCell>
  <TableCell>
{booking.reservation.rooms[r].name}
  </TableCell>
  <TableCell>
    <b>Room {r+1} Board</b>
  </TableCell>
  <TableCell>
   {booking.reservation.rooms[r].board}
  </TableCell>
  <TableCell>
    <b>Room {r+1} Pax</b>
  </TableCell>
  <TableCell>
{booking.reservation.rooms[r].paxes[0].name+" "+booking.reservation.rooms[r].paxes[0].surname}
   
  </TableCell>
</TableRow>)
});


    //   let approve = (
    //     <Button color="success" onClick={this.Approve} className="mr-2">
    //       Approve
    //     </Button>
    //   );
    //   if (deal.state === 1)
    //     approve = (
    //       <b className="mr-3 mt-3" style={{ color: "green", fontSize: "18px" }}>
    //         Approved
    //       </b>
    //     );

    //   let decline = (
    //     <Button color="danger" onClick={this.Decline}>
    //       Decline 
    //     </Button>
    //   );
    //   if (deal.state === 2)
    //     decline = (
    //       <b className="mr-3 mt-3" style={{ color: "red", fontSize: "18px" }}>
    //         Declined{" "}
    //       </b>
    //     );

    //   if (deal.state === 4) {
    //     approve = decline = null;
    //   }
      
      const lastProcess = this.lastProcess(booking); //getting the first one(who created it)
      
      // let originalPrice = (
      //   {deal.originalPrice}
      // );
  
      // if (this.state.editing === true)
      //   price = (
      //     <div className="row">
      //       <input
      //         type="number"
      //         className="form-control col-8 ml-2"
      //         value={this.state.price}
      //         onChange={this.onChange}
      //       />
      //       <button
      //         className="col-3 btn btn-primary btn-sm ml-2"
      //         onClick={this.Edit}
      //       >
      //         Done
      //       </button>
      //     </div>
      //   );
      bookingRender = (
        <Card>
          <CardBody>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell>
                    <b>Hotel Name</b>
                  </TableCell>
                  <TableCell>
            {booking.reservation.property.translations[0].name}
                  </TableCell>
                  <TableCell>
                    <b>Hotel Address</b>
                  </TableCell>
                  <TableCell>
                    {booking.reservation.property.translations[0].address}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Holder First Name</b>
                  </TableCell>
                  <TableCell>{booking.reservation.holderFirstName}</TableCell>
                  <TableCell>
                    <b>Holder Last Name</b>
                  </TableCell>
                  <TableCell>{booking.reservation.holderLastName}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>CheckIn Date</b>
                  </TableCell>
                  <TableCell>
                    {moment
                      .utc(booking.reservation.checkInDate)
                      .local()
                      .format("DD MMM YYYY")}
                  </TableCell>
                  <TableCell>
                    <b>CheckOut Date</b>
                  </TableCell>
                  <TableCell>
                    {moment
                      .utc(booking.reservation.checkOutDate)
                      .local()
                      .format("DD MMM YYYY")}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Price</b>
                  </TableCell>
                  <TableCell>{booking.totalPrice}</TableCell>
                  {/* <TableCell>
                    <b>Original Price</b>
                  </TableCell>
                  <TableCell>{!this.state.editing ? this.state.price:<div className="row"> */}
          {/* <input
            type="number"
            className="form-control col-8 ml-2"
            //  defaultValue={deal.originalPrice}
           // defaultValue={this.state.price}
              value={this.state.price}
             onChange={this.onChange}
          />
          <button
            className="col-3 btn btn-primary btn-sm ml-2"
             onClick={()=>this.Edit(deal.id)}
          >
            Done
          </button>
        </div>}</TableCell> */}
                 
                </TableRow>

                {rooms}


                <TableRow>
                  <TableCell>
                    <b>Reservation</b>
                  </TableCell>
                  <TableCell>{booking.reservation.reservationType}</TableCell>
                  <TableCell>
                    <b>Reference</b>
                  </TableCell>
                  <TableCell>
                    {booking.reference}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>User FullName</b>
                  </TableCell>
                  <TableCell>{lastProcess.user.fullName}</TableCell>
                  <TableCell>
                    <b>User Email</b>
                  </TableCell>
                  <TableCell>{lastProcess.user.email}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Reservation Email</b>
                  </TableCell>
                  <TableCell>{booking.reservation.email}</TableCell>
                  <TableCell>
                    <b>Emails</b>
                  </TableCell>
                  <TableCell>
                    <Link to={`/emails/${booking.id}`}>--></Link>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
            {/* {(booking.state===0||booking.state===1||lastProcess.state===2) &&      <div className="mt-5 float-right row">
              {approve}
              {decline}
              
     <p  onClick={()=>this.setState({editing:true},()=>{console.log(this.state)})} className="mr-2">
            Edit Price
        </p>
      
            </div>} */}
          </CardBody>
        </Card>
      );
    }
    return (
      <React.Fragment>
        <Heading title="Timeline" textAlign="center" />
        <GridContainer>
          <GridItem xs={12}>{bookingRender}</GridItem>
        </GridContainer>
      </React.Fragment>
    );
  }
}

export default BookingPage;
