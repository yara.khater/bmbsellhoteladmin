import React from "react";
import * as actions from "../../store/actions/index";
import PropTypes from "prop-types";

import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Heading from "components/Heading/Heading.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import Success from "components/Typography/Success.jsx";
import Danger from "components/Typography/Danger.jsx";
import config from "../../config";

import Details from "@material-ui/icons/Details";
import history from "../../history";

import { connect } from "react-redux";
import axios from "axios";
//import { Button } from "@material-ui/core";
import Button from "components/CustomButtons/Button.jsx";
import moment from "moment";

class DealPage extends React.Component {
  state = {
    emails: null
  };

  componentDidMount() {
    axios
      .get(`${config.host}/Emails?dealid=${this.props.match.params.dealid}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") //the token is a variable which holds the token
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ emails: res.data });
        // this.setState({GetEmails:res.data},() => console.log(this.state))
      });
  }

  render() {
    let x = 1;
    if (this.state.emails) {
      // x=this.state.emails[0];

      var emails = this.state.emails.map(email => {
        // console.log(email)

        return (
          <Card>
            <CardBody>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <b>From</b>
                    </TableCell>
                    <TableCell>{email.fromEmail}</TableCell>
                    <TableCell>
                      <b>To </b>
                    </TableCell>
                    <TableCell>{email.toEmail}</TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell>
                      <b>Date Sent</b>
                    </TableCell>
                    <TableCell>
                      {moment
                        .utc(email.dateSent)
                        .local()
                        .format("DD MMM YYYY")}
                    </TableCell>
                    <TableCell>
                      <b>Subject</b>
                    </TableCell>
                    <TableCell>{email.subject}</TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell>
                      <b>Body</b>
                    </TableCell>
                    <TableCell>
                      {/* {email.body} */}
                      <a
                        target="_blank"
                        href={`${config.host}/emails/message?messageId=${
                          email.id
                        }`}
                      >
                        Email Message Details
                      </a>
                    </TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell>
                      <b>Attachments</b>
                    </TableCell>
                    {email.attachments.map(attach => {
                      return (
                        <TableCell>
                        
                          <a
                           target="_blank"
                            href={
                              config.attach_url +
                              "/" +
                              email.id +
                              "/" +
                              attach.id +
                              "/" +
                              attach.fileName
                            }
                          >
                            Link
                          </a>
                        </TableCell>
                      );
                    })}
                  </TableRow>
                </TableBody>
              </Table>
            </CardBody>
          </Card>
        );
      });
    }

    return (
      <React.Fragment>
        <Heading title="Timeline" textAlign="center" />
        <GridContainer>
          <GridItem xs={12}>{emails}</GridItem>
        </GridContainer>
      </React.Fragment>
    );
  }
}

export default DealPage;
