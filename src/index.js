
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import Dashboard from "./layouts/Dashboard";
// import { Provider } from 'react-redux';
// import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
// import thunk from 'redux-thunk';
import DealPage from "views/Pages/DealPage.jsx";
import indexRoutes from "routes/index.jsx";
import ProtectedRoute from "./ProtectedRoutes";

import { AuthProvider } from "./AuthContext.js";
import {DealsProvider} from './DealsContext.js'
import "assets/scss/material-dashboard-pro-react.css?v=1.4.0";
import LoginPage from "views/Pages/LoginPage.jsx";
import Header from "./views/Components/Header";
// import dealsReducer from './store/reducers/deals';
 import history from './history';
 require('dotenv').config();
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//const hist = createBrowserHistory();
// const rootReducer = combineReducers({
//   // signin: signinReducer,
//    deals: dealsReducer,
//   // hotels:hotelsReducer,
//   // room:roomReducer
// });

// const store = createStore(rootReducer, composeEnhancers(
//   applyMiddleware(thunk)
// ));

ReactDOM.render(

  <DealsProvider>
    <AuthProvider>
      <Router history={history}>
      <Switch>
           {/* <ProtectedRoute path={`/deals/:id`} component={DealPage}/> */}
        <ProtectedRoute path="/dashboard" component={Dashboard} />
        <ProtectedRoute path="/deals" component={Dashboard} />
        <ProtectedRoute path="/emails" component={Dashboard} />
        {indexRoutes.map((prop, key) => {
          return (
            <Route path={prop.path} component={prop.component} key={key} />
          );
        })}
      </Switch>
      </Router>
 
    </AuthProvider>
    </DealsProvider>
 ,
  document.getElementById("root")
);
