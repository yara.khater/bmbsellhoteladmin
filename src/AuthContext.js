import React from "react";
import axios from "axios";
import history from "./history";
// import history from 'history';
import config from "config";
const AuthContext = React.createContext();

class AuthProvider extends React.Component {
  state = {
    isAuth: localStorage.getItem("token") !== null
    //  token:null
  };
  login = (email, password) => {
    axios({
      method: "GET",
      url: `${config.host}/Token`,
      params: { email: email, password: password, screen: "admin" }
    })
      .then(r => {
        console.log(r);
        this.setState({ isAuth: true, token: r }, () => {
          console.log(this.state);
        });
        localStorage.setItem("token", r.data.token);
        history.push("/");
      })
      .catch(err => {
        console.log("errrrrr");
        console.log(this.state);
      });
    console.log("hi");
  };
  logout = () => {
    localStorage.removeItem("token");
  };
  render() {
    return (
      <AuthContext.Provider
        value={{
          isAuth: this.state.isAuth,
          login: this.login,
          logout: this.logout
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}
const AuthConsumer = AuthContext.Consumer;
export { AuthProvider, AuthConsumer };
