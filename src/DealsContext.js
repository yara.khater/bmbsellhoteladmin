import React from 'react';
import axios from 'axios';
import history from './history';
// import history from 'history';
import config from 'config'
const DealsContext = React.createContext();


class DealsProvider extends React.Component {


    state = { Deals: [],
  
    }
    find=(str)=>{

        axios
      .get(`${config.host}/Deals/Search`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token") 
        },
        params: {
          searchString: str,
        
        }
      })
      .then(res => {
        console.log(res.data);
       
      })
      .catch(err => {
         
        console.log(err);
      });
    
      }
   
    render() {
  
      return (
        <DealsContext.Provider
          value={{ deals: this.state.deals,
            find: this.find}}
        >
     
          {this.props.children}
        </DealsContext.Provider>
      )
    }
  }
  const DealsConsumer = DealsContext.Consumer
  export { DealsProvider, DealsConsumer }