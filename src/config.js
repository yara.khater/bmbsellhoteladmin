
export default {
    host: process.env.REACT_APP_API_HOST,
    attach_url:process.env.REACT_APP_ATTACHMENT_URL
}